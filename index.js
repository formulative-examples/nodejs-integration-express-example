const express = require('express');
const mortgageCalculations = require('./mortgageCalculations.js');

const app = express();
const port = 3000;

// parse request body as json in /api endpoints
app.use('/api', express.json());

app.post('/api/calculate', (req, res) => {
  if (!req.body || typeof req.body.calculation !== 'string') {
    return res.status(400).send({
      message: 'missing property "calculation"'
    });
  }
  const [calcGroup, calc] = req.body.calculation.split('/');
  if (!calcGroup || !calc) {
    return res.status(400).send({
      message: 'property "calculation" must include the calculationGroup id and ' +
        'calculation id separated by a slash character e.g.: "group/calculation"'
    });
  }
  if (!mortgageCalculations[calcGroup] || !mortgageCalculations[calcGroup][calc]) {
    return res.status(404).send({
      message: 'calculation not found'
    });
  }
  try {
    const result = mortgageCalculations[calcGroup][calc](req.body.params);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

// everything else is handled by static file serving from the folder 'public'
app.use('/', express.static('public'));

// start listening
app.listen(port, () => {
  console.log(`Example app started at http://localhost:${port}`);
});
